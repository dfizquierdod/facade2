/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facadedfid;
import tclases.*;

/**
 *
 * @author estudiantes
 */
public class ClaseFacade {
    private ClaseAutodidacta claseAutodidacta;
    private ClaseGrabada claseGrabada;
    private ClasePresencial clasePresencial;
    private ClaseVirtual claseVirtual;
    
    public ClaseFacade(){
        claseAutodidacta = new ClaseAutodidacta();
        claseGrabada = new ClaseGrabada();
        clasePresencial = new ClasePresencial();
        claseVirtual = new ClaseVirtual();        
    }
    
    
    public void registraClaseAutodidacta(){
        claseAutodidacta.registrarClase();
    }
    public void registraClaseGrabada(){
        claseGrabada.registrarClase();
    }
    public void registraClasePresencial(){
        clasePresencial.registrarClase();
    }
    public void registraClaseVirtual(){
        claseVirtual.registrarClase();        
    }
            
    
    
    
}
