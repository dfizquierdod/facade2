/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facadedfid;

import java.util.Scanner;

/**
 *
 * @author estudiantes
 */
public class FacadeDfid {

    /**
     * @param args the command line arguments
     */
    
    
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        ClaseFacade claseFacade = new ClaseFacade();
        
        int opcion = 0;

        
        System.out.println("Seleccione el tipo de Clase a registrar");
        System.out.println("1 para clase autodidacta ");
        System.out.println("2 para clase grabada ");
        System.out.println("3 para clase presencial ");
        System.out.println("4 para clase virtual ");
        System.out.print("O3pcion: ");
        opcion = reader.nextInt();
        System.out.println("");
        if (opcion == 1) {
            claseFacade.registraClaseAutodidacta();
        }else if(opcion ==2){
            claseFacade.registraClaseGrabada();
        }else if (opcion == 3) {
            claseFacade.registraClasePresencial();
        }else if (opcion == 4) {
            claseFacade.registraClaseVirtual();
        }else{
            System.out.println("no selecciono ninguna opcion ");
        }              
    }
    
}
